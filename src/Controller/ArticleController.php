<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Menu;
use App\Form\NewsletterType;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ArticleType;
use App\Form\CommentType;
use Doctrine\ORM\EntityManagerInterface;
use App\Twig\TexyExitension;
use App\Entity\User;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

//use Texy\Texy;

class ArticleController extends AbstractController
{
    /**
     * @Route("/user/article/add", name="article_index")
     */
    public function index(Request $request, AuthenticationUtils $authenticationUtils) : Response
    {
        $item = new Article();
        $form = $this->createForm(ArticleType::class, $item);
        $form->handleRequest($request);
        if( $form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $item = new Article();
            $perex = $form['perex']->getData();
            $description = $form['description']->getData();
            $content = $form['content']->getData();
            $type = $form['type']->getData();
            $title = $form['title']->getData();
            $url = $form['urlseo']->getData();
            $image = $form['image']->getData();

            $item->setPerex($perex);
            $item->setDescription($description);
            $item->setTitle($title);
            $item->setContent($content);
            $item->setType($type);
            $item->setUrlseo($url);
            $item->getLastmod(NULL);
            $item->setIp($request->getClientIp());
            $item->setImage($image);
            try {
                $row = $this->getDoctrine()->getRepository(Article::class)->findAll();
                $this->addFlash('success', 'Article was successfully added');
                $em->persist($item);
                $em->flush();
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

        }
        $row = $this->getDoctrine()->getRepository(User::class)->findAll();


        $article = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        $newsletter = $this->createForm(NewsletterType::class);
        $row = $this->getDoctrine()->getRepository(User::class)->findAll();
        /*if($this->getUser()) {
            return $this->redirectToRoute('article_index', [
                'article' => $row,
                'form' => $form->createView(),
                'user' => $this->getUser()->id,
                'news' => $newsletter->createView(),
            ]);
        }*/

           if($this->getUser()) {
               return $this->render('article/index.html.twig', [
                   'article' => $article,
                   'form' => $form->createView(),
                   'news' => $newsletter->createView(),
                   'user' => $row

               ]);
           }


    }


  /**
     * @Route("admin/article/list", name="admin_list")
     */
    public function list(Request $request, ArticleRepository $articleRepostory)
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->findAll();
        $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        $newsletter = $this->createForm(NewsletterType::class);
        return $this->render('article/list.html.twig', [
            'item' => $article,
            'article' => $count,
            'news' => $newsletter->createView()
        ]);
    }

    /**
     * @Route("/blog", name="blog_index")
     */

    public function blog(Request $request, ArticleRepository $articleRepostory, AuthenticationUtils $utils)
    {
        $configItem =
            [
                'headerImage' => 'https://images.unsplash.com/photo-1454165804606-c3d57bc86b40?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
                'footer' => ''
            ];
        try {
            $blog = $this->getDoctrine()->getRepository(Article::class)->selectAllBlogArticle('blog');
	    //dd($blog);
            $article = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
            $count = $this->getDoctrine()->getRepository(User::class)->findAll();
            $newsForm = $this->createForm(NewsletterType::class);
            $error = $utils->getLastAuthenticationError();

            //dd('uuiuuiu');
            $params = [
                'items' => $blog,
                'article' => $article,
                'news' => $newsForm->createView(),
                'config' => $configItem

            ];

            if($this->getUser())
            {
                $row = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->id);
                $params = [
                    'user' => $row,
                    'error' => $error,
                    'article' => $article,
                    'items' => $blog
                ];
            }

        } catch(\Exception $e)
        {
            throw new \Exception($e->getMessage());
        }
        
        //$user = $this->getDoctrine()->getRepository(User::class)->find($user->getId());
        $article = $this->getDoctrine()->getRepository(Article::class)->findAll();        
        if(!$blog) { $blog = []; } else {  $blog; }
        return $this->render('article/blog/index.html.twig', $params);
    }

    /**
     * @Route("preview/{url}/detail", name="article_detail")
     * @param Request $request
     * @return Response
     */

     public function detail(Request $request) : Response
     {

         $commentForm = $this->createForm(CommentType::class);
         $newsletter = $this->createForm(NewsletterType::class);
         $article = $this->getDoctrine()->getRepository(Article::class)->find($request->get('id'));
         $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();

        $params = [
            'id' => $article->getId(),
            'article' => $count,
            'item' => $article,
            'commentForm' => $commentForm->createView(),
            'news' => $newsletter->createView()
        ]; 
        return $this->render('article/detail.html.twig', $params);
     }

      /**
     * @Route("/admin/article/{id}/edit", name="admin_article_edit")
     */
    public function edit(Article $article, Request $request, EntityManagerInterface $em)
    {
       // dd('rrrr');
        $row = $this->getDoctrine()->getRepository(Article::class)->find($request->get('id'));
        $form = $this->createForm(ArticleType::class, $article);
        $newsletter = $this->createForm(NewsletterType::class);
        $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        $form->handleRequest($request);
        $article->setLastmod(new \DateTime);
        $article->setIp($request->getClientIp());
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();
            $this->addFlash('success', 'Article Updated! Inaccuracies squashed!');
            return $this->redirectToRoute('admin_article_edit', [
                'id' => $article->getId(),
            ]);
        }
        return $this->render('article/edit.html.twig', [
            'editForm' => $form->createView(), 
            'item' => $row,
            'news' => $newsletter->createView(),
            'article' => $count
        ]);
    }

    /**
     * @Route("/changelog", name="changelog_index")
     */
    public function changelog(Request $request)
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->selectAllBlogArticle('changelog');
        $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        $newsletter = $this->createForm(NewsletterType::class);

        return $this->render('changelog/index.html.twig', [
            'item' => $article,
            'article' => $count,
            'news' => $newsletter->createView()
        ]);
    }


    /**
     * @Route("/ajax", name="_recherche_ajax")
     */
    public function ajaxAction(Request $request)
    {
       $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        $newsletter = $this->createForm(NewsletterType::class);
       $param = [
         'news' => $newsletter->createView(),
         'article' => $count
       ];
        $info = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {
            $jsonData = array();
            $idx = 0;
            foreach($info as $data) {
                $temp = array(
                    'name' => $request->getClientIp(),
                    'address' => $request->getHttpHost(),
                );

                $jsonData[$idx++] = $temp;
            }
            return new JsonResponse($jsonData);
        } else {
            return $this->render('log/log.html.twig', $param);
        }
    }

}
