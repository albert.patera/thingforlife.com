<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Menu;
use App\Form\NewsletterType;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ArticleType;
use App\Form\CommentType;
use Doctrine\ORM\EntityManagerInterface;
use App\Twig\TexyExitension;
use App\Entity\User;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

//use Texy\Texy;

class CommentController extends AbstractController
{
    /**
     * @Route("/user/comment/add", name="comment_index")
     */
    public function index(Request $request, AuthenticationUtils $authenticationUtils) : Response
    {
        $item = new Comment();
        $commentForm = $this->createForm(CommentType::class, $item);
        $commentForm->handleRequest($request);
        if( $commentForm->isSubmitted() && $commentForm->isValid())
        {
            //dd('jj');
            $em = $this->getDoctrine()->getManager();

            $item = new Comment();
            $title_data = $commentForm['title']->getData();
            $content_data = $commentForm['content']->getData();
            
            
            $item->setTitle($title_data);
            $item->setContent($content_data);
            $item->setDateCreated(new \DateTime());
            $item->setLastmod(NULL);
            $item->setArticleId($request->get('id'));
            $item->setAuthor($this->getUser()->id);
        


            try {
                $this->addFlash('success', 'Comment was successfully added');
                $em->persist($item);
                $em->flush();
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

        }
        //$row = $this->getDoctrine()->getRepository(User::class)->findAll();


        //$article = $this->getDoctrine()->getRepository(Comment::class)->selectCommentByArticle();
        
            return $this->render('form/commentForm.html.twig', 
                ['commentForm' => $commentForm->createView()]
            );
            //return $this->redirectToRoute('comment_index');
        
        //$newsletter = $this->createForm(NewsletterType::class);
        //$row = $this->getDoctrine()->getRepository(User::class)->findAll();


    }    

}
