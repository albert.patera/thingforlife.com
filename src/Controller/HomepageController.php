<?php
namespace App\Controller;

use App\Entity\Menu;
use App\Entity\Article;
use App\Entity\User;
use App\Form\NewsletterType;
use App\Repository\MenuRepository;
use App\Repository\UserRepository;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomepageController extends AbstractController
{ 
    

	public function index(Request $request, MenuRepository $menuRepository, ArticleRepository $articleRepository) : Response
    {
        $menu = $this->getDoctrine()->getRepository(Menu::class)->findAll();
        $newsletterForm = $this->createForm(NewsletterType::class);

        $article = $this->getDoctrine()->getRepository(Article::class)->findAll();
        //$configItem =
          //  [
            //    'headerImage' => 'https://images.unsplash.com/photo-1454165804606-c3d57bc86b40?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
              //  'footer' => ''
            //];
	$params = [
		'menu' => $menu,
		'article' =>  $article,
	];

	return  $this->render('homepage/index.html.twig', $params);
    }


	 /**
     * @Route("/preview", name="demo")
     */
    public function demo(Request $request)
    {
        //$posts = $this->getDoctrine()
          //  ->getRepository(Post::class)
            //->findLatest();
	$articles = $this->getDoctrine()
			->getRepository(Article::class)
			->selectAllBlogArticle('blog');
	//dd($text->getId());
	//$article = $this->getDoctrine()
	//		->getRepository(Article::class)
	//		->find($text->getId());
	$author = $this->getDoctrine()
			->getRepository(User::class)
			->selectOneUser(14);
        return $this->render('devel/test.blog.html.twig', [
        //    'id' => $text->getId(),
		'article' => $articles,
            'people' => $author,
	  //  'detail' => $article,
        ]);
    }
    /**
     * @Route("/access-denied", name="access")
     */
    function accessDenied() : Response
    {
        return $this->redirectToRoute('blog_index');
    }


}
