<?php


namespace App\Controller;


use App\Entity\Menu;
use App\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends AbstractController
{
    public function index(Request $request, MenuRepository $menuRepository) : Response
    {
        $menu = $this->getDoctrine()->getRepository(Menu::class)->findAll();
        return $this->render('menu/index.html.twig', [
            'menu' => $menu
        ]);
    }
}