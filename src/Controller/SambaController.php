<?php
namespace App\Controller;

use App\Entity\Menu;
use App\Entity\Article;
use App\Entity\User;
use App\Form\NewsletterType;
use App\Repository\MenuRepository;
use App\Repository\UserRepository;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;



class SambaController extends AbstractController
{ 
    /**
     * @Route("/admin/user/folder-permission", name="remote_action")
     */

     public function index() : Response
     {
        $filesystem = new Filesystem();
        $current_dir_path = getcwd();
        //dump($current_dir_path);
        $mk = $filesystem->touch('/tmp/photos.txt');
        $check = $filesystem->exists('/tmp/photos');
        //dump($mk, $check);
        //dd($filesystem);
        $temp_file = tempnam(sys_get_temp_dir(), 'Tux');

        $config = [
            'headerImage' => 'https://images.unsplash.com/photo-1541728472741-03e45a58cf88?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1632&q=80',
            'mainTitle' => 'folder permissions' 
        ];

        $params = [
            'config' => $config
        ];
    

        //dump($temp_file);
        //dump(sys_get_temp_dir());
        try {
            $filesystem->mkdir(sys_get_temp_dir().'/'.random_int(0, 1000));
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at ".$exception->getPath();
        }
        
        
        return $this->render('permissions/samba.html.twig', $params); 
     }

     
}
