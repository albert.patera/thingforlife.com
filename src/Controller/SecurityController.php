<?php

namespace App\Controller;

use App\Form\NewsletterType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Entity\Article;

class SecurityController extends AbstractController
{

    /**
     * @Route("/secret", name="app_profile")
     */

     public function admin(Request $request)
     {
         return $this->render('security/login.html.twig');
     }
    
    
    /**
     * @Route("/profile/{id}/", name="target_path")
     */

     public function adminRoot(User $user,  UserRepository $userRepository,  Request $request, AuthenticationUtils $userAuth)
     {
    
         $article = $this->getDoctrine()->getRepository(Article::class)->findAll();
         $row = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->id);
         $error = $userAuth->getLastAuthenticationError();
       // last username entered by the user
        $lastUsername = $userAuth->getLastUsername();
        //dd($user->getId());
        if(!$lastUsername)
        {
            return $this->redirectToRoute('app_login');
        }
        //dd($user);
        $params = [
            'error' => $error, 
            'lastusername' => $lastUsername,
            'user' => $row,
            'article' => $article
        ];
        //dd($user->getId());
        return $this->render('security/admin/admin.html.twig', $params);
     }

    /**
     * @Route("/user", name="user_auth")
     */

    public function userProfile(Request $request, AuthenticationUtils $authenticationUtils) : Response
    {
        if(!$this->getUser()) {
            return $this->redirectToRoute("app_login");
        }
        $article = $this->getDoctrine()->getRepository(Article::class)->findAll();
        $row = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->id);
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if(!$lastUsername)
        {
            return $this->redirectToRoute('app_login');
        }

        if($this->getUser()->getRoles()[0] == "ROLE_ADMIN")
        {
            $renderMenu = 'security/menu/normalUser.html.twig';
        }
        //dd($user);
        $paramsAdmin = [
            'error' => $error,
            'lastusername' => $lastUsername,
            'user' => $row,
            'article' => $article,
            'menu' => ''
        ];

        return $this->render('security/admin/admin.html.twig', $paramsAdmin);
    }

    /**
     * @Route("/auth", name="firstAuthentification")
     */

    public function authentificate() : Response
    {
        if($this->getUser()) {
            if($this->getUser()->getRoles()[0] === 'ROLE_ADMIN') {
               // return $this->redirectToRoute('admin_auth', [
                        //'id' => $this->getUser()->id,
                        // 'url' => $this->getUser()->urlseo
                 //   ]
                //);
            }
            return $this->redirectToRoute('target_path', [
                'url' => $this->getUser()->urlseo,
                'id' => $this->getUser()->id,

            ]);
        }
    }


    public function adminProfile(Request $request, AuthenticationUtils $authenticationUtils)
    {

        $article = $this->getDoctrine()->getRepository(Article::class)->findAll();
        $row = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->id);
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        if(!$lastUsername)
        {
            return $this->redirectToRoute('app_login');
        }


        //dd($user);
        $params = [
            'error' => $error,
            'lastusername' => $lastUsername,
            'user' => $row,
            'article' => $article
        ];
        //dd($user->getId());
        return $this->render('security/user/index.html.twig', $params);
    }





    /**
     * @Route("/internal-login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request): Response
    {
        if(!$this->getUser()) {
            $this->redirectToRoute('first_login_process');
        }
        $newsletter = $this->createForm(NewsletterType::class);
        $count = $this->getDoctrine()->getRepository(Article::class)->findAll();

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if($this->getUser()) {
           if($this->getUser()->getRoles()[0] === 'ROLE_USER') {
               return $this->redirectToRoute('user_auth', [
                   //'id' => $this->getUser()->id,
                  // 'url' => $this->getUser()->urlseo
               ]
               );
           }
           //if($this->getUser()->getRoles()[0] === 'ROLE_ADMIN') {
             //  return $this->redirectToRoute('admin');
           //}
            return $this->redirectToRoute('target_path', [
                'url' => $this->getUser()->urlseo,
                'id' => $this->getUser()->id,

            ]);
        }


        //dd($this->getUser());
         return $this->render('security/login.html.twig', [
           'news' => $newsletter->createView(),
             'last_username' => $lastUsername,
        'error' => $error,
        'user' => 4,
        'article' => $count
        ]);

    }

    /**
     * @Route("/acount/active", name="active-acount")
     */

    public function activeAcount() : Response
    {
        return $this->redirectToRoute('blog');
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

  
}
