<?php
namespace App\Controller;

use App\Entity\Article;
use App\Entity\FileUpload;
use App\Entity\UploadFile;
use App\Entity\User;
use App\Form\UploadType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use phpseclib\Net\SFTP;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\String\Slugger\SluggerInterface;
#use phpseclib\Net\SFTP;



class UploadFileController extends AbstractController
{
    /* @var UploadFile @inject */
    public $fileData;

    /**
     * @Route("/user/upload/file", name="uploader")
     */
    public function new(Request $request, SluggerInterface $slugger, EntityManagerInterface $em)
    {

        $allFiles = $this->getDoctrine()->getRepository(UploadFile::class)->findAll();
	$allFilesByUser = $this->getDoctrine()->getRepository(UploadFile::class)->findFilesCountByUser($this->getUser()->id);
        $fileByType = $this->getDoctrine()->getRepository(UploadFile::class)->findFilesByType('pdf');
        $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        $projectRoot = $this->getParameter('kernel.project_dir');
        $product = new UploadFile();
        $form = $this->createForm(UploadType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $brochureFile */
            $brochureFile = $form->get('attachment')->getData();
            $imagesFile = $form->get('pictures')->getData();
            //benefitAttachment-lkoixwiv30-5ed9f2fc1c62f601617b10b2
            /* @var UploadFile $fileData; @inject*/
                $fileData = new UploadFile();
                $fileData->getId();
            //dd($fileData->getId());
            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded

            if($imagesFile) {
                $originalFilename = pathinfo($imagesFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $product->setType('jpg');
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imagesFile->guessExtension();
                //dd($form['uploadPath']->getData());
                // Move the file to the directory where brochures are stored
                try {
                    $imagesFile->move(
                    //$this->getParameter('brochures_directory'),
                    // $this->getDoctrine()->getRepository(UploadFile::class)->find(['uploadPath' => ]),
                    //$this->get
                        $this->getUser()->urlseo . '/' .  $form['uploadPath']->getData() . '/',
                        $newFilename

                    );

                    //dd($fileData->getId());
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                $product->setFileName($newFilename);
                $product->setUserId($this->getUser()->id);
                //$product->setType('pdf');
                //if($imagesFile) {
                $product->setType('jpg');
                //}
                $product->setUploadPath($form['uploadPath']->getData());

                $user = new User();
                $user->setProfileImage('image');
                //dd($user)
                ;

                //dd($product);
                try {
                    //dd($product);
                    $em->persist($product);
                    $em->flush();

                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }
            if ($brochureFile) {
                //dd($fileData->getId());
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL

                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$brochureFile->guessExtension();
                //dd($form['uploadPath']->getData());
                // Move the file to the directory where brochures are stored
                try {
                    $brochureFile->move(
                       //$this->getParameter('brochures_directory'),
                       // $this->getDoctrine()->getRepository(UploadFile::class)->find(['uploadPath' => ]),
                        //$this->get
                         $this->getUser()->urlseo . '/' .  $form['uploadPath']->getData() . '/',
                          $newFilename

                    );

                    //dd($fileData->getId());
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents

                $product->setFileName($newFilename);
                $product->setUserId($this->getUser()->id);
                //$product->setType('pdf');
                //if($imagesFile) {
                $product->setType('pdf');
                //}
                $product->setUploadPath($form['uploadPath']->getData());
                //dd($product);
                try {
                  //dd($product);
                    $em->persist($product);
                    $em->flush();

                } catch (\Exception $e)
                {
                    throw new \Exception($e->getMessage());
                }

            }

            // ... persist the $product variable or any other work

            return $this->redirectToRoute('file_upload_list');
        }
        $params  = [
            'up_form' => $form->createView(),
            'article' => $count,
            'file_param' => $fileByType,
            'image_path' => $projectRoot . '/upload/brochures/',
            'user_path' => $this->getUser()->urlseo,
            'pathToFolder' => $product->getUploadPath(),
            'files' => $allFilesByUser

        ];
        //dd($product->getUploadPath());
       // dd($params);

        return $this->render('security/admin/user/upload/upload.html.twig',  $params);

    }
    /**
     * @Route("/user/upload/list", name="file_upload_list")
     */

    public function file_upload_list(Request $request)
    {
        //dd($this->getParameter('pathToFolder'));
        $fileByType = $this->getDoctrine()->getRepository(UploadFile::class)->findFilesByType('pdf');
        //$article = $this->getDoctrine()->getRepository(UploadFile::class)->find($request->get('id'));
        //dd($article);

        $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        $projectRoot = $this->getParameter('kernel.project_dir');
        $fileSortByUser = $this->getDoctrine()
            ->getRepository(UploadFile::class)
            ->findFilesByTypeAndUser('pdf', $this->getUser()->id);
        $imgSortByUser = $this->getDoctrine()
            ->getRepository(UploadFile::class)
            ->findFilesByTypeAndUser('jpg', $this->getUser()->id);


        $params = [
            'file_data' => $fileSortByUser,
            'file_images_data' => $imgSortByUser,
            'article' => $count,
            'user_path' => $this->getUser()->urlseo,
            'pathToFolder' => '',
            'image_data' => $imgSortByUser

        ];
       // dd($params);
        return $this->render('security/admin/user/upload/list.html.twig', $params);


        //dd()


    }
    /**
     * @Route("/user/sftp", name="sftp")
     */

    public function connect(AuthenticationUtils $userAuth)
    {

        $filesystem = new Filesystem();
        $userToUploadPath = $this->getUser()->urlseo;
        $params = [
            'user_path' => $userToUploadPath
        ];
        $filesystem->mkdir($this->getUser()->urlseo, 0777);
        $filesystem->mkdir($this->getUser()->urlseo. '/automobil', 0777);
        $filesystem->mkdir($this->getUser()->urlseo. '/dum', 0777);
        $filesystem->mkdir($this->getUser()->urlseo. '/zahrada', 0777);
	$fiesystem->mkdir("../../../../../home/" . $this->getUser()->urlseo . '/files', 0777);

        //$filesystem->mkdir('../../automobil', 0777);
        return $this->render('security/admin/clients/accounts.html.twig', $params);
    }


}
