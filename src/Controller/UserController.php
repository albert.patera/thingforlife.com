<?php
namespace App\Controller;

use App\Entity\Article;
use App\Form\NewsletterType;
use App\Form\RegistrationType;
use App\Entity\User;
use App\Form\UploadType;
use App\Form\UserProflleType;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController extends AbstractController
{ 
    
    /**
     * @Route("/registration", name="user_registration")
     */
    public function index(Request $request, AuthenticationUtils $userInfo, UserPasswordEncoderInterface $encoder) : Response
    {


        $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        $form = $this->createForm(RegistrationType::class);
        $form->handleRequest($request);

        //$dumplicityEmail = $this->getDoctrine()->getRepository(User::class)->find(['email' => $form['email']->getData()]);

       // dd($dumplicityEmail);

        $params = [
            'form' => $form->createView(),
            'article' => $count
        ];

        if($form->isSubmitted() && $form->isValid())
        {

            $em = $this->getDoctrine()->getManager();
            $user = new User();
            $user->setUrlseo(strtolower($form['firstname']->getData()) .
                '-' . strtolower($form['lastname']->getData()));

            $user->setFirstname($form['firstname']->getData());
            $user->setEmail($form['email']->getData());
            $user->setActive(false);
            $user->setBan(false);
            $user->setLastname($form['lastname']->getData());
            $user->setBeforeDegree($form['beforeDegree']->getData());
            $user->setIp($request->getClientIp());
            $user->setAfterDegree($form['afterDegree']->getData());
            $user->setRoles(['ROLE_USER']);
		
		$encoded = $encoder->encodePassword($user,$form['password']->getData());
		$user->setPassword($encoded);
            try {
                $dumplicityEmail = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $form['email']->getData()]);

                if($dumplicityEmail) {
                    $this->addFlash(
                        'danger',
                        'You choose email witch is already exists. Please choose different email. Thanks'
                    );
                } else {
                    $this->addFlash(
                        'success',
                        'You are successfully registered !'
                    );

                    $em->persist($user);
                    $em->flush();
                }

            } catch (\Exception $e)
            {
                throw new \Exception($e->getMessage());
            }
        }

        return $this->render('register/index.html.twig', $params);
    }

     /**
     * @Route("{url}/{id}/user/list", name="user_list")
     */

     public function userlist(AuthenticationUtils $authenticationUtils, Request $request) : Response
     {
        //dd($this->getUser()->id);
         if(!$this->getUser()) {
             return $this->redirectToRoute('app_login');
         }
         $users = $this->getDoctrine()->getRepository(User::class)->selectUser();
         $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
         $newsletter = $this->createForm(NewsletterType::class);
            $params = [
                'user' => $users,
                'article' => $count,
                'news' => $newsletter->createView(),
                'url' => $this->getUser()->urlseo,
                'id' => $this->getUser()->id
            ];
         return $this->render('security/admin/user/list.html.twig', $params);
     }

    /**
     * @Route("/login/step-1", name="first_login_process")
     */

    public function loginProcess() : Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->selectUser();
        $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        $newsletter = $this->createForm(NewsletterType::class);
        $params = [
            'user' => $users,
            'article' => $count,
            'news' => $newsletter->createView(),
        ];
        if($this->getUser())
        {
            $params = [
                'user' => $users,
                'article' => $count,
                'news' => $newsletter->createView(),
                'url' => $this->getUser()->urlseo,
                'id' => $this->getUser()->id
            ];
            return $this->render('security/login/step-1.html.twig', $params);
        }
        return $this->render('security/login/step-1.html.twig', $params);


    }

    /**
     * @Route("/profile/user/edit", name="edit-profile")
     */

    public function editProfile() : Response
    {
        $count = $this->getDoctrine()->getRepository(Article::class)->selectAllArticle();
        $uploadImage = $this->createForm(UploadType::class);
        $row = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->id);
        $editForm = $this->createForm(RegistrationType::class);
        $userProfileEditForm = $this->createForm(UserProflleType::class);
        //if($editForm->isSubmitted() && $editForm->isValid()) {

        //}
        return $this->render('security/admin/user/profile/index.html.twig', [
            'article' => $count,
            'up_form' => $uploadImage->createView(),
            'edit_profile_form' => $editForm->createView(),
            'user' => $row,
            'user_profile_form' => $userProfileEditForm->createView()
        ]);



    }


}
