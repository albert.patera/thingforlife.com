<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

class FileUpload
{
    // ...

    /**
     * @ORM\Column(type="string")
     */
    private $attachment;

    public function getAttachment()
    {
        return $this->attachment;
    }

    public function setBrochureFilename($attachment)
    {
        $this->attachment = $attachment;

        return $this;
    }
}