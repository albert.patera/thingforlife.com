<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MenuRepository")
 */
class Menu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_menu;

    /**
     * @ORM\Column(type="text")
     */
    private $show_name;

    /**
     * @ORM\Column(type="text")
     */
    private $internal_name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $redirect_to;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $parent_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdMenu(): ?int
    {
        return $this->id_menu;
    }

    public function setIdMenu(?int $id_menu): self
    {
        $this->id_menu = $id_menu;

        return $this;
    }

    public function getShowName(): ?string
    {
        return $this->show_name;
    }

    public function setShowName(string $show_name): self
    {
        $this->show_name = $show_name;

        return $this;
    }

    public function getInternalName(): ?string
    {
        return $this->internal_name;
    }

    public function setInternalName(string $internal_name): self
    {
        $this->internal_name = $internal_name;

        return $this;
    }

    public function getRedirectTo(): ?string
    {
        return $this->redirect_to;
    }

    public function setRedirectTo(?string $redirect_to): self
    {
        $this->redirect_to = $redirect_to;

        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    public function setParentId(?int $parent_id): self
    {
        $this->parent_id = $parent_id;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
