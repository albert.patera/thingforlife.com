<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('title', TextType::class, ['attr' => ['class' => 'text-input form-control col-md-4', 'placeholder' => 'Fill the title here']])
        ->add('perex', TextType::class, ['attr' => ['class' => 'text-input form-control col-md-4', 'placeholder' => 'perex']])
        ->add('description', TextareaType::class, ['attr' => ['class' => 'text-input form-control col-md-12', 'placeholder' => 'Fill the title here']])
        ->add('content', TextareaType::class, ['attr' => ['class' => 'text-input form-control col-md-12', 'placeholder' => 'Fill the title here']])
        ->add('type', TextType::class, ['attr' => ['class' => 'text-input form-control col-md-12', 'placeholder' => 'Fill the type here']])
        ->add('urlseo', TextType::class, ['attr' => ['class' => 'text-input form-control col-md-12', 'placeholder' => 'Fill the type here']])
        ->add('image', TextType::class, ['attr' => ['class' => 'text-input form-control col-md-12', 'placeholder' => 'Fill the type here']])
        ->add('submit', SubmitType::class, ['attr' => ['class' => 'text-input btn btn-primary col-md-12 form-control', 'value' => 'Aded article for agree']])
          ;
    }
}
