<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('title', TextType::class, ['attr' => ['class' => 'form-control col-sm-12']])
        ->add('content', TextAreaType::class, ['attr' => ['class' => 'form-control col-sm-12']])
        ->add('postComment', SubmitType::class, ['attr' => ['class' => 'btn btn-primary col col-md-12', 'value' => 'submit registration']])
            ;
    }

   // public function configureOptions(OptionsResolver $resolver)
    //{
      //  $resolver->setDefaults([
        //    'data_class' => RegistrationType::class,
        //]);
    //}
}
