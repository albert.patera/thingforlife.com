<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Email;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('beforeDegree', TextType::class,
                [   'label_translation_parameters' => ['super'],
                    'required' => false,
                    'attr' => ['placeholder' => 'Bc.', 'class' => 'form-control col-md-2']]
            )
            ->add('firstname', TextType::class,
                [ 'required' => false,
                    'attr' => ['placeholder' => 'John', 'class' => 'form-control col-md-5']])
            ->add('lastname', TextType::class,
                [ 'required' => false,
                    'attr' => ['reguired' => false, 'placeholder' => 'Doe', 'class' => 'form-control col-md-4']])
            ->add(
                'afterDegree', TextType::class,
                [   'required' => false,
                    'attr' => ['placeholder' => 'MBA', 'class' => 'form-control col-md-2', 'required' => 'false']])
            ->add('email', EmailType::class, ['attr' => [ 'placeholder' => 'email@exampe.com', 'class' => 'form-control col-md-5']])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password']])

                ->add('email', RepeatedType::class, [
                                'type' => EmailType::class,
                                'invalid_message' => 'The email fields must match.',
                                'options' => ['attr' => ['class' => 'email-field']],
                                'required' => true,
                                'first_options'  => ['label' => 'E-mail'],
                                'second_options' => ['label' => 'Repeat email']])


            ->add('register', SubmitType::class, ['attr' => ['class' => 'btn btn-info btn-lg col col-md-12', 'value' => 'submit registration']]);
    }

   // public function configureOptions(OptionsResolver $resolver)
    //{
      //  $resolver->setDefaults([
        //    'data_class' => RegistrationType::class,
        //]);
    //}
}
