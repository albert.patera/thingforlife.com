<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Email;

class UserProflleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profileImage', TextType::class,
                [   'required' => false,
                    'attr' => ['class' => 'form-control col-md-2']]
            )

            ->add('save', SubmitType::class, ['attr' => ['class' => 'btn btn-info btn-lg col col-md-12', 'value' => 'submit registration']]);
    }

    // public function configureOptions(OptionsResolver $resolver)
    //{
    //  $resolver->setDefaults([
    //    'data_class' => RegistrationType::class,
    //]);
    //}
}
