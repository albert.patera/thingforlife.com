<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200407180051 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_a (id INT AUTO_INCREMENT NOT NULL, ip LONGTEXT NOT NULL, password LONGTEXT NOT NULL, email LONGTEXT NOT NULL, active TINYINT(1) NOT NULL, ban TINYINT(1) NOT NULL, username VARCHAR(255) NOT NULL, before_degree VARCHAR(255) DEFAULT NULL, after_degree VARCHAR(255) DEFAULT NULL, firstname LONGTEXT NOT NULL, lastname LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user DROP passwor, DROP actibe, CHANGE email email VARCHAR(180) NOT NULL, CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', CHANGE password password VARCHAR(255) NOT NULL, CHANGE before_degree before_degree VARCHAR(255) DEFAULT NULL, CHANGE after_degree after_degree VARCHAR(255) DEFAULT NULL, CHANGE firstname firstname LONGTEXT NOT NULL, CHANGE active active TINYINT(1) DEFAULT NULL, CHANGE ban ban TINYINT(1) DEFAULT NULL, CHANGE ip ip LONGTEXT NOT NULL, CHANGE urlseo urlseo LONGTEXT NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('ALTER TABLE article CHANGE ip ip LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE menu DROP type, CHANGE show_name show_name LONGTEXT NOT NULL, CHANGE internal_name internal_name LONGTEXT NOT NULL, CHANGE description description VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user_a');
        $this->addSql('ALTER TABLE article CHANGE ip ip TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE menu ADD type TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE show_name show_name TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE internal_name internal_name TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE description description TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74 ON user');
        $this->addSql('ALTER TABLE user ADD passwor TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, ADD actibe INT DEFAULT NULL, CHANGE email email TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE roles roles TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE password password TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE before_degree before_degree VARCHAR(10) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE firstname firstname TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE after_degree after_degree VARCHAR(10) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE active active INT DEFAULT NULL, CHANGE ban ban INT DEFAULT NULL, CHANGE ip ip TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE urlseo urlseo TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`');
    }
}
