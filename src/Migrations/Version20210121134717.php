<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210121134717 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, perex LONGTEXT NOT NULL, description LONGTEXT NOT NULL, content LONGTEXT NOT NULL, type VARCHAR(255) DEFAULT NULL, title LONGTEXT DEFAULT NULL, urlseo LONGTEXT NOT NULL, lastmod DATETIME DEFAULT NULL, ip LONGTEXT NOT NULL, image LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu (id INT AUTO_INCREMENT NOT NULL, id_menu INT DEFAULT NULL, show_name LONGTEXT NOT NULL, internal_name LONGTEXT NOT NULL, redirect_to LONGTEXT DEFAULT NULL, parent_id INT DEFAULT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE upload_file (id INT AUTO_INCREMENT NOT NULL, image VARCHAR(255) DEFAULT NULL, user_id INT DEFAULT NULL, file_name LONGTEXT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, upload_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, before_degree VARCHAR(255) NOT NULL, firstname LONGTEXT NOT NULL, lastname LONGTEXT NOT NULL, after_degree VARCHAR(255) NOT NULL, active TINYINT(1) DEFAULT NULL, ban TINYINT(1) DEFAULT NULL, ip LONGTEXT NOT NULL, profile_image LONGTEXT DEFAULT NULL, urlseo LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_a (id INT AUTO_INCREMENT NOT NULL, ip LONGTEXT NOT NULL, password LONGTEXT NOT NULL, email LONGTEXT NOT NULL, active TINYINT(1) NOT NULL, ban TINYINT(1) NOT NULL, username VARCHAR(255) NOT NULL, before_degree VARCHAR(255) DEFAULT NULL, after_degree VARCHAR(255) DEFAULT NULL, firstname LONGTEXT NOT NULL, lastname LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE menu');
        $this->addSql('DROP TABLE upload_file');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_a');
    }
}
