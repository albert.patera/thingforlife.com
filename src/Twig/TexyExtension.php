<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Texy\Texy;
use Twig\TwigFilter;

class TexyExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('texy', [$this, 'texyLoad']),
        ];
    }

    public function texyLoad($value)
    {
        $icons = [
            ':-)' => 'happy-1.png',
            ':-(' => 'sad-1.png',
            ';-)' => 'wink.gif',
            ':-D' => 'biggrin.gif',
            '8-O' => 'eek.gif',
            '8-)' => 'cool.gif',
            ':-?' => 'confused.gif',
            ':-x' => 'mad.gif',
            ':-P' => 'razz.gif',
            ':-|' => 'neutral.gif',
        ];
        $texy = new Texy;
        $texy->htmlOutputModule->lineWrap = 180;
        $texy->emoticonModule->root = 'images';
        $texy->emoticonModule->class = 'smiley';
        $texy->allowed['emoticon'] = true;
        $texy->emoticonModule->icons[':-)'] = 'happy-1.png';
        $texy->emoticonModule->icons[':-('] = 'sad-1.png';
        
        return $texy->process($value);
    }
}